#include <iostream>
using namespace std;

int main(){
        setlocale(LC_ALL, "portuguese");
	int i, n, peso, num_peso;
	float nota, med, med_peso, cal_peso;
	cout << "Quantas notas? ";
	cin >> n;
	for (i=0; i<n; i++){
		cout << "Nota: ";
		cin >> nota;
		med += nota;
		cout << "Peso: ";
		cin >> peso;
		num_peso += peso;
		cal_peso += (nota * peso);
	}
	med /= n;
	cout << "A média é: " << med << endl;
	med_peso = cal_peso / num_peso;
	cout << "A média com peso é: " << med_peso << endl;

	system("pause");
        return 0;
}
