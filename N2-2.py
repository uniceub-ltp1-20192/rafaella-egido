#define a quantidade de cebolas
cebolas = 300
#define a quantidade de cebolas na caixa
cebolas_na_caixa = 120
#define quantas cebolas cabem em casa caixa
espaco_caixa = 5
#define a quantidade de caixas
caixas = 60
#define a quantidade de cebolas fora da caixa
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
#define a quantidade de caixas vazias
caixas_vazias = int(caixas - (cebolas_na_caixa/espaco_caixa))
#define a quantiidade de caixas necessarias para colocar todas as cebolas em caixas
caixas_necessarias = int(cebolas_fora_da_caixa / espaco_caixa)
#Imprime a quantidade de cebolas encaixotadas
print("Existem", cebolas_na_caixa, "cebolas encaixotadas")
#Imprime a quantidade de cebolas fora das caixas
print("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
#Imprime quantas cebolas cabem em cada caixa
print("Em cada caixa cabem", espaco_caixa, "cebolas")
#Imprime a quantidade de caixas vazias
print("Ainda temos,", caixas_vazias, "caixas vazias")
#Imprime a quantidade de caixas necessarias para empacotar todas as cebolas
print("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
