def common(a, b):
    a_set = set(a)
    b_set = set(b)
    if (a_set & b_set):
        print (a_set & b_set)

a = ["ABC", "DEF", "GHI", "YWZ"]
b = ["CEB", "YWZ", "ABC"]
print("Primeira lista: ", a)
print("Segunda lista: ", b)
print("Elementos comuns: ")
common(a, b)
