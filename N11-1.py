#N7.3
guests = ['Michael Jackson', 'Amy Winehouse', 'Whitney Houston']
print(guests[0] + ", você está convidado para o meu jantar.")
print(guests[1] + ", você está convidada para o meu jantar.")
print(guests[2] + ", você está convidada para o meu jantar.")
print("\n" + guests[0] + " não poderá comparecer ao jantar.\n")
guests[0] = 'Lady Gaga'
print(guests[0] + ", você está convidada para o meu jantar.")
print(guests[1] + ", você está convidada para o meu jantar.")
print(guests[2] + ", você está convidada para o meu jantar.")
print("\nEu encontrei uma mesa de jantar maior.\n")
guests.insert(0, 'Elvis Presley')
guests.insert(2, 'Beyoncé')
guests.append('Rihanna')
print(guests[0] + ", você está convidado para o meu jantar.")
print(guests[1] + ", você está convidada para o meu jantar.")
print(guests[2] + ", você está convidada para o meu jantar.")
print(guests[3] + ", você está convidada para o meu jantar.")
print(guests[4] + ", você está convidada para o meu jantar.")
print(guests[5] + ", você está convidada para o meu jantar.")
#N11.1
print("\nOs primeiros 3 itens da lista são: ", guests[0:3])
print("Os últimos 3 itens da lista são: ", guests[-3:])
print("Os 3 itens no meio da lista são: ", guests[2:5])
